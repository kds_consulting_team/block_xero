view: ar_history {
  label: "Account Receivable History"
  measure: overdue_amount {
    description: "Amount overdue at the time"
    type: number
    sql: SUM(IFF(${invoice.due_date}<${calendar.date_date},${invoice.total_amount_dimension},NULL)) ;;
    value_format_name: usd
    drill_fields: [invoice.invoice_list*]
  }

  measure: overdue_count {
    description: "Number of invoices overdue at the time"
    type: number
    sql:SUM(IFF(${invoice.due_date}<${calendar.date_date},1,NULL)) ;;
    drill_fields: [invoice.invoice_list*, is_overdue]
  }

  measure: is_overdue {
    description: "Drill in formatting of overdue_count measure"
    type: number
    sql:SUM(IFF(${invoice.due_date}<${calendar.date_date},1,0)) ;;
    value_format: "[>0]\"true\";[=0]\"false\""
  }

  measure: 60_overdue_amount {
    description: "Amount overdue at the time"
    type: number
    sql: SUM(IFF(${calendar.date_date}::DATE-${invoice.due_date}::DATE>60,${invoice.total_amount_dimension},NULL)) ;;
    value_format_name: usd
    drill_fields: [invoice.invoice_list*]
  }

  measure: 60_overdue_count {
    description: "Number of invoices overdue at the time"
    type: number
    sql:SUM(IFF(${calendar.date_date}::DATE-${invoice.due_date}::DATE>60,1,NULL)) ;;
    drill_fields: [invoice.invoice_list*]
  }
}
